*** Settings ***
Library         SeleniumLibrary
Resource        recursos.robot

*** Test Cases ***
001 IR al blog de Winston Castillo
    open Homepage
    Title Should Be     Hola Mundo!
    Set Focus To Element    xpath=/html/body/div[1]/div/div[2]/a[1]
    Click Link      xpath=/html/body/div[1]/div/div[2]/a[1]
    wait until Element is Visible   xpath=/html/body/div[1]/div[2]/div
    Title Should Be     Winston Castillo – Un sitio para comunicarse
    Close Browser

002 Abrir Ventana Modal
    open Homepage
    Title Should Be     Hola Mundo!
    Set Focus To Element    xpath=/html/body/div[1]/div/div[2]/a[2]
    Click Link      xpath=/html/body/div[1]/div/div[2]/a[2]
    Title Should Be     Hola Mundo!
    wait until Element is Visible   xpath=//*[@id="exampleModal"]/div/div/div[1]
    Close Browser